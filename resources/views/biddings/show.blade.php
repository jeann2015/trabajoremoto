@extends('layouts.main')

@section('content')
    <div class="show-container-flex">
        {{-- Informacion del empleador--}}
        <div class="show-info-employer">
            <div class="show-info-employer-logo">
                <img src="{{ $bidding->company_logo }}" alt="">
            </div>
            <span class="show-info-employer-name">{{$bidding->company}}</span>
            <span class="show-info-employer-social">
                @if(isset($bidding->company_url) && $bidding->company_url <> '')
                    <a href="{{ $bidding->company_url }}" target="_blank">
                        <i class="fa fa-2x fa-external-link" aria-hidden="true"></i>
                    </a>
                @endif
            </span>
        </div>

        {{-- Informacion de la oferta --}}
        <div class="show-info-job">
            <span class="show-info-job-title">{{ strip_tags($bidding->title) }}</span>
            <div style="width: 100%">
                <span class="show-info-job-posted pull-left">
                    @if(isset($bidding->tipotrabajo) && isset($bidding->type))
                        {{ $bidding->tipotrabajo }} - {{ $bidding->type }}
                    @elseif(isset($bidding->type))
                        {{ $bidding->type }}
                    @elseif(isset($bidding->type))
                        {{ $bidding->type }}
                    @endif
                </span>
                <span class="show-info-job-posted pull-right">Publicado desde {{ Carbon\Carbon::parse($bidding->created_at)->format('d/m/Y') }}</span>
            </div>
            <h4>{{ strip_tags($bidding->title) }}</h4>
            <hr>
            <h5 class="show-info-job-subtitle"><i class="fa fa-search" aria-hidden="true"></i> Que buscamos en ti?</h5>
            <span class="show-info-job-mainbody">{{ strip_tags($bidding->description) }}</span>
            <hr>
            <h5 class="show-info-job-subtitle"><i class="fa fa-plane" aria-hidden="true"></i> Como aplicar?</h5>
            <span class="show-info-job-mainbody">{{ strip_tags($bidding->how_to_apply) }}</span>
            <hr>
            <a href="{{ $bidding->url }}" target="_blank">
                <i class="fa fa-2x fa-external-link" aria-hidden="true"> See the Bidding</i>
            </a>
                </div>
    </div>
    <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=597923f480bb1d0011ab6e15&product=inline-share-buttons"></script>
@endsection