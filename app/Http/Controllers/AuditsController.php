<?php

namespace Trabajosremoto\Http\Controllers;

use Illuminate\Http\Request;
use Trabajosremoto\Audits;

class AuditsController extends Controller
{
    /**
     * AuditsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $audits_news = new Audits;

        $iduser = \Auth::id();
        $url = $request->path();

        $audits_news->save_audits('Views Auditoria');

        $audits = \DB::table('audits')
            ->join('users','audits.users_id','=','users.id')
            ->select('audits.*', 'users.name')
            ->orderBy('id', 'desc')
            ->paginate(15);
        return view('audits.index',compact('iduser','audits'));
    }
}
