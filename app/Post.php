<?php

namespace Trabajosremoto;

use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * @method static Search($get)
 */
class Post extends Model
{
    use Sluggable;
    use SluggableScopeHelpers;

    protected $fillable = [
                            'title',
                            'que_busca',
                            'que_ofrece',
                            'como_aplicar',
                            'tipotrabajo',
                            'tiempotrabajo',
                            'destacado',
                            'adminStatus',
                            'status',
                            'vigente_hasta',
                            'visto',
                            'slug'
                        ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the visits for the post.
     */
    public function visitas()
    {
        return $this->hasMany(Visita::class);
    }

    /**
     * @param $query
     * @param $text
     * @return mixed
     */
    public function scopeSearch($query, $text)
    {

        $search = "%".trim($text)."%";
        $q = $query->where('title', 'like', $search);
        return $q;
    }
}
