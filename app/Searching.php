<?php

namespace Trabajosremoto;

use Exception;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Searching extends Model
{
    protected $table="searchings";

    protected $fillable = [
        'ip',
        'code_country',
        'country',
        'state',
        'currency',
        'search'
    ];

    /**
     * @param $ip
     * @param $searching
     */
    static public function SetSearching($ip,$searching)
    {
        $search = [];
        $info = geoip($ip);
        try{
            $search['ip'] = $ip;
            $search['code_country'] = $info->iso_code;
            $search['country'] = $info->country;
            $search['state'] = $info->state_name;
            $search['currency'] = $info->currency;
            $search['search'] = $searching;

            Searching::create($search);
        } catch (Exception $e) {
            Log::info($e);
            return;
        }

    }
}
