<?php

use Illuminate\Database\Seeder;
use Trabajosremoto\Visita;

class VisitasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Visita::class, 300)->create();
    }
}
