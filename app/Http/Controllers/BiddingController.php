<?php

namespace Trabajosremoto\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Trabajosremoto\Audits;
use Trabajosremoto\Bidding;

class BiddingController extends Controller
{
    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['raiz', 'jobs']);
    }

    public function show(Request $request)
    {
        $bidding = Bidding::find($request->id);
        if (is_null($bidding)) {
            return redirect()->route('home')->withErrors(['Error' => 'Algo salio mal... No conseguimos este trabajo']);
        }
        if (Auth::check()) {
            Audits::save_audits('Vio sus post ' . $request->id);
        }
        return view('biddings.show', compact("bidding"));
    }
}
