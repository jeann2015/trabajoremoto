@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h5> {{ __('profile.My Profile') }}</h5></div>
                    <div class="panel-body">
                        {!! Form::open(['route' => 'employer.update', 'method' => 'PUT', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data' ]) !!}
                            {{ Form::token() }}

                            <h5>{{ __('profile.Principal Information') }}</h5>
                            {{-- Logo --}}
                            <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                                {!! Form::label('logo', __('profile.Logo'), ['class' => 'col-md-2 control-label']) !!}

                                <div class="col-md-10">
                                    @if(isset($user->logo))
                                            <img width="80px" height="80px" src="{{ Storage::url( $user->logo) }}">
                                    @endif
                                    {!! Form::file('logo', ['class' => 'form-control', 'id'=>'logo', '']) !!}
                                    <span>Max 2 MB | JPEG JPG PNG | Max 250 x 250</span>

                                    @if ($errors->has('logo'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('logo') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            {{-- Status --}}
                            @if(Auth::user()->isAdmin)
                            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                {!! Form::label('estatus', __('profile.Status'), ['class' => 'col-md-2 control-label']) !!}

                                <div class="col-md-10">
                                    {{ Form::select('status', [
                                                    0 => __('profile.No Active'),
                                                    1 => __('profile.Active')
                                                ], $user->status, ['class' => 'form-control input-profile']) }}
                                    @if ($errors->has('status'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            @endif

                            {{-- Email --}}
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                {!! Form::label('email', __('profile.Email'), ['class' => 'col-md-2 control-label']) !!}

                                <div class="col-md-10">
                                    {!! Form::email('email', $user->email, ['class' => 'form-control input-profile', 'id'=>'email', 'autocomplete'=>'off', 'required', 'title' => 'Correo electronico principal del empleador' ]) !!}

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            {{-- Empresa --}}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                {!! Form::label('name', __('profile.Company'), ['class' => 'col-md-2 control-label']) !!}

                                <div class="col-md-10">
                                    {!! Form::text('name', $user->name, ['class' => 'form-control input-profile', 'id'=>'name', 'autocomplete'=>'off', 'required', 'title' => 'Nombre del empleador' ]) !!}

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            {{-- Slogan --}}
                            <div class="form-group{{ $errors->has('slogan') ? ' has-error' : '' }}">
                                {!! Form::label('slogan', __('profile.Slogan'), ['class' => 'col-md-2 control-label']) !!}

                                <div class="col-md-10">
                                    {!! Form::text('slogan', $user->slogan, ['class' => 'form-control input-profile', 'id'=>'slogan', 'autocomplete'=>'off', 'require', 'title' => 'Lema comercial. Ejemplo: "Tu mejor opcion..!"']) !!}

                                    @if ($errors->has('slogan'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('slogan') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            {{-- bio --}}
                            <div class="form-group{{ $errors->has('bio') ? ' has-error' : '' }}">
                                {!! Form::label('bio', __('profile.Us'), ['class' => 'col-md-2 control-label']) !!}

                                <div class="col-md-10">
                                    {!! Form::textarea('bio', $user->bio, ['class' => 'form-control input-profile textarea', 'id'=>'bio', 'autocomplete'=>'off', 'require', 'placeholder' => 'Somos una empresa dedicada a la comercializacion de productos informaticos... etc.']) !!}
                                    @if ($errors->has('bio'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('bio') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <hr>
                            <h5>{{ __('profile.Security') }}</h5>

                            {{-- Password --}}
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                {!! Form::label('password', __('profile.Password'), ['class' => 'col-md-2 control-label']) !!}

                                <div class="col-md-10">
                                    {!! Form::password('password', ['class' => 'form-control input-profile', 'id'=>'password', "required", 'title' => 'Contresaña']) !!}

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            {{-- Confirm password --}}
                            <div class="form-group">
                                {!! Form::label('password-confirm', __('profile.Password Confirm'), ['class' => 'col-md-2 control-label']) !!}

                                <div class="col-md-10">
                                    {!! Form::password('password_confirmation', ['class' => 'form-control input-profile', 'id'=>'password-confirm', "required", 'title' => 'Contresaña']) !!}

                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <hr>
                            <h5>{{ __('profile.Social Network') }}</h5>

                            {{-- URL --}}
                            <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
                                {!! Form::label('url', 'URL', ['class' => 'col-md-2 control-label']) !!}

                                <div class="col-md-10">
                                    {!! Form::text('url', $user->url, ['class' => 'form-control input-profile', 'id'=>'url', 'autocomplete'=>'off', 'title' => 'Ejemplo: https://tuempresa.com/' ]) !!}

                                    @if ($errors->has('url'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            {{-- facebook --}}
                            <div class="form-group{{ $errors->has('facebook') ? ' has-error' : '' }}">
                                {!! Form::label('facebook', 'Facebook', ['class' => 'col-md-2 control-label']) !!}

                                <div class="col-md-10">
                                    {!! Form::text('facebook', $user->facebook, ['class' => 'form-control input-profile', 'id'=>'facebook', 'autocomplete'=>'off', 'title' => 'Ejemplo: https://facebook.com/nombredetuempresa' ]) !!}

                                    @if ($errors->has('facebook'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('facebook') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            {{-- twitter --}}
                            <div class="form-group{{ $errors->has('twitter') ? ' has-error' : '' }}">
                                {!! Form::label('twitter', 'Twitter', ['class' => 'col-md-2 control-label']) !!}

                                <div class="col-md-10">
                                    {!! Form::text('twitter', $user->twitter, ['class' => 'form-control input-profile', 'id'=>'twitter', 'autocomplete'=>'off', 'title' => 'Ejemplo: https://facebook.com/nombredetuempresa']) !!}

                                    @if ($errors->has('twitter'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('twitter') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            {{-- linkedin --}}
                            <div class="form-group{{ $errors->has('linkedin') ? ' has-error' : '' }}">
                                {!! Form::label('linkedin', 'Linkedin', ['class' => 'col-md-2 control-label']) !!}

                                <div class="col-md-10">
                                    {!! Form::text('linkedin', $user->linkedin, ['class' => 'form-control input-profile', 'id'=>'linkedin', 'autocomplete'=>'off', 'title' => 'Ejemplo: https://linkedin.com/nombredetuempresa']) !!}

                                    @if ($errors->has('linkedin'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('linkedin') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            {{-- instagram --}}
                            <div class="form-group{{ $errors->has('instagram') ? ' has-error' : '' }}">
                                {!! Form::label('instagram', 'Instagram', ['class' => 'col-md-2 control-label']) !!}

                                <div class="col-md-10">
                                    {!! Form::text('instagram', $user->instagram, ['class' => 'form-control input-profile', 'id'=>'instagram', 'autocomplete'=>'off', 'title' => 'Ejemplo: https://instagram.com/nombredetuempresa' ]) !!}

                                    @if ($errors->has('instagram'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('instagram') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <hr>

                            <div class="form-group">
                                <div class="col-md-12" align="center">
                                    <button type="reset" class="btn btn-default">
                                       {{ __('profile.Delete') }}
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('profile.Update') }}
                                    </button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
