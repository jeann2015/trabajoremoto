<?php

return [
    'Title' => 'Titulo de la Oferta',
    'TitleDescription' => 'Ejemplo: Desarrollador Experimentado en Java EE',
    'Conditions' => 'Condiciones',
    'Select' => 'Seleccione una opcion',
    'Hired' => 'Contratado',
    'Undefined' => 'Indefinido',
    'Freelancer' => 'Freelance',
    'Per Hours' => 'Por Hora',
    'Middle Time' => 'Medio Tiempo',
    'Fulltime' => 'Fulltime',
    'Contract' => 'Contrato',
    'Looking' => 'Que Buscamos Nosotros?',
    'DescriptionLooking' => 'Escribe aqui las habilidades y destrezas que debe tener el aspirante, las funciones que cumpira asi como tambien cualquier otra informacion que consideres que pueda ser de utilidad para el aspirante',
    'We Offer' => 'Que Ofrecemos?',
    'DescriptionWeOffer' => 'Coloca aqui la infomacion sobre los beneficios que ofreces al apirante asi como tambien sobre el paquete salarial. Intenta ser lo mas interesante posible para el aspirante.',
    'How Apply' => 'Como Aplicar?',
    'HowApplyDescription' => 'Expresa aqui los principales datos de contactos para que los aspirantes puedan enviar sus CVs, por ejemplo: "Por favor enviar CV a correo@dominio.com"',
    'Create Bidding' => 'Crear Oferta',
    'Highlight' => ' Quieres Resaltar la Oferta?',

];