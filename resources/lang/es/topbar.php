<?php

return [
    'Profile'=>'Mi Perfil',
    'List Collaborator' => 'Lista de Empleados',
    'Logs' => 'Auditorías',
    'Types Bidding' => 'Tipos de Ofertas',
    'Create Bidding' => 'Crear Oferta',
    'My Bidding' => 'Mis Ofertas',
    'Logout' => 'Salir',
    'Sign In' => 'Entrar',
    'Register' => 'Registrarse',
];