<?php

namespace Trabajosremoto;

use Illuminate\Database\Eloquent\Model;

class Word extends Model
{
    protected $table="words";

    protected $fillable = [
        'description',
        'status'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function AccountBiddings(){
        return $this->belongsToMany('Trabajosremoto\AccountBidding');
    }
}
