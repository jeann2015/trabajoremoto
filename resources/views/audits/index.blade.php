@extends('layouts.main')
@section('content')

    <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-block">
                            <table class="table table-striped table-hover table-responsive">
                                <thead>
                                <tr>
                                    <td colspan="4">
                                        <div align="center">
                                            <h1>{{ __('audit.Audits') }}</h1>
                                        </div>

                                        <div class="pull-right">
                                            <a href="{{ route('home') }}" class="btn btn-default" role="button"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ __('audit.Back') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>{{ __('audit.Id') }}</th>
                                    <th>{{ __('audit.User') }}</th>
                                    <th>{{ __('audit.Description') }}</th>
                                    <th>{{ __('audit.DateTime') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($audits as $audit)
                                    <tr>
                                        <td>{{ $audit->id }}</td>
                                        <td>{{ $audit->name }}</td>
                                        <td>{{ $audit->description }}</td>
                                        <td>{{ Carbon\Carbon::parse($audit->created_at)->format('l jS \\of F Y h:i:s A')  }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div align="center" style="width: 100%;">
                                {{ $audits->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection