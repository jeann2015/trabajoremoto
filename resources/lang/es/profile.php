<?php

return [
    'My Profile' => 'Mi Perfil',
    'Principal Information' => 'Informacion General',
    'Logo' => 'Logo',
    'Status' => 'Estado del usuario',
    'Email' => 'Correo Electronico',
    'Company' => 'Empresa',
    'Slogan' => 'Lema',
    'Us' => 'Nosotros',
    'Password' => 'Clave',
    'Password Confirm' => 'Confirmar Clave',
    'Social Network' => 'Redes Sociales',
    'Delete' => 'Borrar',
    'Update' => 'Actualizar',
    'Active' => 'Activo',
    'No Active' => 'Inactivo',
];