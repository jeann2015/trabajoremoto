<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('tipotrabajo')->nullable();   // contrato, indeterminado, proyecto
            $table->string('tiempotrabajo')->nullable(); // por horas, medio-tiempo, full-time
            $table->text('que_busca');
            $table->text('que_ofrece');
            $table->text('como_aplicar');
            $table->boolean('destacado')->default(false);
            $table->boolean('adminStatus')->default(true);
            $table->boolean('status')->default(true);
            $table->dateTime('vigente_hasta');
            $table->integer('visto')->default(0);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
