<?php

namespace Trabajosremoto\Http\Middleware;

use Closure;
use Exception;
use Trabajosremoto\Post;

class ownerPost
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $post = Post::findBySlug($request->slug);
        if (is_null($post)){
            return redirect()->route('home')->withErrors(['Error' => 'Algo salio mal... No conseguimos este trabjo']);
        }

        if ($request->user()->id != $post->user_id){
            return redirect()->route('job.show', $post->slug)->withErrors(['Error' => 'No tienes privilegios para modificar este trabajo']);
        }
        return $next($request);
    }
}