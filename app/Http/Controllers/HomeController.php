<?php

namespace Trabajosremoto\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Trabajosremoto\Audits;
use Trabajosremoto\Post;
use Trabajosremoto\Searching;
use Trabajosremoto\VisitaGeneral;

class HomeController extends Controller
{
    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['raiz', 'jobs']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        Audits::save_audits('Entro al Home de la aplicacion');
        $iduser = Auth::id();
        $posts = Post::Search($request->get('search'))->where('user_id', '=', Auth::user()->id)
            ->orderBy('vigente_hasta', 'asc')
            ->get();


        return view('home', compact('iduser','posts'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function raiz(Request $request)
    {
        $idUser = Auth::id();

        if($request->search<>"") {

            $search_processing = explode( " ",$request->search);

        }else{$search_processing[0]="%";}


        $date_search = Carbon::now()->subDays(10)->toDateTimeString();

        $posts_local = DB::table('posts')
            ->join('users','posts.user_id','=','users.id')
            ->select(
                'posts.title','posts.slug','posts.tipotrabajo','posts.id as id',
                'tiempotrabajo','que_busca','que_ofrece','como_aplicar',
                'destacado','adminStatus',
                'posts.status','vigente_hasta','visto','user_id',\DB::raw('1 as type_post'),
                'users.name','users.slogan','users.facebook','users.url','users.twitter',
                'users.instagram','users.logo','posts.created_at'

            )
            ->where('posts.status', '=', 1)
            ->where('posts.adminStatus', '=', 1)
            ->where('posts.created_at', '>=', $date_search)
            ->Where('posts.title', 'like', "%" . $search_processing[0] . "%")
            ->orderByDesc('posts.created_at');

        if($search_processing <> "%") {
            foreach ($search_processing as $item) {
                $posts_local->OrWhere('posts.title', 'like', "%" . $item . "%");
            }
        }

        $posts_local = $posts_local->get();

        $posts_external = DB::table('biddings')->select(DB::raw(
                "title,'-' as slug,type as tipotrabajo,'-' as tiempotrabajo,'-' as que_busca,
                description as que_ofrece,'how_to_apply' as como_aplicar,'0' as destacado,
                0 as adminStatus,1 as status,'2018-07-29 11:00:11' as vigente_hasta,'0' as visto,
                '1' as user_id,2 as type_post,'External' as name,location as slogan,'Ninguno' as facebook, 
                biddings.url,'Ninguno' as twitter, 'Ninguno' as instagram, company_logo as logo,created_at,
                biddings.id as id"
            )
        )
            ->where('created_at', '>=', $date_search)
            ->Where('title', 'like', "%" . $search_processing[0] . "%")
            ->orderByDesc('created_at');

        if($search_processing <> "%" ) {
            foreach ($search_processing as $item) {
                $posts_external->OrWhere('title', 'like', "%" . $item . "%");
            }
        }

        $posts_external = $posts_external->get();

        $posts = $posts_local->merge($posts_external);

        $posts = $this->paginate($posts->sortByDesc('created_at'));

        $ip = $request->server('REMOTE_ADDR');
        VisitaGeneral::guardarVisita($ip);


        if($request->search<>""){
            Searching::SetSearching($ip,$request->search);
        }
        return View('principal', compact('idUser','posts'));
    }

    /**
     * @param $items
     * @param int $perPage
     * @param null $page
     * @param array $options
     * @return LengthAwarePaginator
     */
    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

}
