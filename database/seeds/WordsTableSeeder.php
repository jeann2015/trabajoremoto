<?php

use Illuminate\Database\Seeder;
use Trabajosremoto\Word;

class WordsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $words = ['java','php','ruby','golang','python','c++','gcc',
        'javascript','g++','react','angular','angular.js','html5',
        'mysql','react-native','android','postgresql','blockchaing',
        'mongodb','laravel','rails','express','solidity','sql',
        'django','flask','sailsjs','hapijs','windows','docker',
        'web3js','swift','kotlin','.NET'];

        foreach ($words as $word){
            Word::create(['description'=>$word,'status'=> 1]);
        }
    }
}
