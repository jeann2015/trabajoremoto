<?php

namespace Trabajosremoto;

use Illuminate\Database\Eloquent\Model;

class Visita extends Model
{
    protected $fillable = [
                            'ip',
                            'code_country',
                            'country',
                            'state',
                            'currency',
                            'post_id',
                        ];

    /**
     * Get the post for this visit.
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
