@extends('layouts.main')

@section('content')
    <div class="show-container-flex">
        {{-- Informacion del empleador--}}
        <div class="show-info-employer">
            <div class="show-info-employer-logo">
                <img src="{{ Storage::url($post->user->logo) }}" alt="">
            </div>
            <span class="show-info-employer-name">{{$post->user->name}}</span>
            <span class="show-info-employer-slogan">{{$post->user->slogan}}</span>
            <span class="show-info-employer-social">
                @if(isset($post->user->facebook))
                    <a href="{{ $post->user->facebook }}" target="_blank">
                        <i class="fa fa-2x fa-facebook-official" aria-hidden="true"></i>
                    </a>
                @endif
                @if(isset($post->user->twitter))
                    <a href="{{ $post->user->twitter }}" target="_blank">
                        <i class="fa fa-2x fa-twitter-square" aria-hidden="true"></i>
                    </a>
                @endif
                @if(isset($post->user->linkedin))
                    <a href="{{ $post->user->linkedin }}" target="_blank">
                        <i class="fa fa-2x fa-linkedin-square" aria-hidden="true"></i>
                    </a>
                @endif
                @if(isset($post->user->instagram))
                    <a href="{{ $post->user->instagram }}" target="_blank">
                        <i class="fa fa-2x fa-instagram" aria-hidden="true"></i>
                    </a>
                @endif

                @if(isset($post->user->url))
                    <a href="{{ $post->user->url }}" target="_blank">
                        <i class="fa fa-2x fa-globe" aria-hidden="true"></i><br>
                        <span class="show-info-employer-social-url">{{ $post->user->url }}</span>
                    </a>
                @endif
            </span>
            <span class="show-info-employer-share">
                <a href="{{ route('employer.profile') }}" class="btn btn-blanco">Editar perfil</a>
            </span>
        </div>

        {{-- Informacion de la oferta --}}
        <div class="show-info-job">
        {!! Form::open(['route' => ['job.update', $post->slug], 'method' => 'PUT', 'class' => 'form-horizontal' ]) !!}
            {{ Form::token() }}
            <span class="show-info-job-posted">Estatus: &nbsp;&nbsp;&nbsp;&nbsp;
                {{ Form::select('status', [
                        1 => 'Activado',
                        0 => 'Desactivado'
                    ], $post->status, ['class' => 'form-control input-profile']) }}
                </span><br>
            {!! Form::text('title', $post->title, ['class' => 'form-control ', 'autocomplete' => 'off']) !!}<br>
            @if ($errors->has('title'))
                <span class="help-block-red">
                    {{ $errors->first('title') }}
                </span>
            @endif
            <div style="width: 100%">
                <span class="show-info-job-posted pull-left">
                    {{ Form::select('tipotrabajo', [
                        'Contratado' => 'Contratado',
                        'Indefinido' => 'Indefinido',
                        'Freelance' => 'Freelance'
                    ], $post->tipotrabajo, ['class' => 'form-control input-profile']) }}-
                    {{ Form::select('tiempotrabajo', [
                        'Por horas' => 'Por horas',
                        'Medio tiempo' => 'Medio tiempo',
                        'Fulltime' => 'Fulltime',
                        'Contrato' => 'Contrato'
                    ], $post->tiempotrabajo, ['class'=>'form-control input-profile']) }}
                </span>
                @if ($errors->has('tipotrabajo'))
                    <span class="help-block-red">
                    {{ $errors->first('tipotrabajo') }}
                </span>
                @endif
                @if ($errors->has('tiempotrabajo'))
                    <span class="help-block-red">
                    {{ $errors->first('tiempotrabajo') }}
                </span>
                @endif<br>
                <span class="show-info-job-posted pull-right">Publicado desde {{ Carbon\Carbon::parse($post->created_at)->format('d/m/Y') }}</span>
            </div>
            <br>
            <br>
            <h5 class="show-info-job-subtitle"><i class="fa fa-search" aria-hidden="true"></i> Que buscamos en ti?</h5>
            {{ Form::textarea('que_busca', $post->que_busca, ['class' => 'form-control show-info-job-mainbody textarea-edit'] ) }}
            @if ($errors->has('que_busca'))
                <span class="help-block-red">
                    {{ $errors->first('que_busca') }}
                </span>
            @endif
            <hr>
            <h5 class="show-info-job-subtitle"><i class="fa fa-trophy" aria-hidden="true"></i> Lo que te ofrecemos.</h5>
            {{ Form::textarea('que_ofrece', $post->que_ofrece, ['class' => 'form-control show-info-job-mainbody textarea-edit'] ) }}
            @if ($errors->has('que_ofrece'))
                <span class="help-block-red">
                    {{ $errors->first('que_ofrece') }}
                </span>
            @endif
            <hr>
            <h5 class="show-info-job-subtitle"><i class="fa fa-plane" aria-hidden="true"></i> Como aplicar?</h5>
            {{ Form::textarea('como_aplicar', $post->como_aplicar, ['class' => 'form-control show-info-job-mainbody textarea-edit'] ) }}
            @if ($errors->has('como_aplicar'))
                <span class="help-block-red">
                    {{ $errors->first('como_aplicar') }}
                </span>
            @endif
            <hr>

            <div class="form-group">
                <div class="col-md-12" align="center">
                    <button type="reset" class="btn btn-default">
                        Borrar
                    </button>
                    <button type="submit" class="btn btn-primary">
                        Actualizar
                    </button>
                </div>
            </div>

        {!! Form::close() !!}
        </div>
    </div>
@endsection