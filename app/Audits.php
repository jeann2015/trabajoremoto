<?php

namespace Trabajosremoto;

use Illuminate\Database\Eloquent\Model;

class Audits extends Model
{
    protected $table="audits";

    /**
     * @param $descriptions
     */
    static public function save_audits($descriptions) {
        $iduser = \Auth::id();
        $audits = new Audits;
        $audits->users_id = $iduser;
        $audits->description = $descriptions;
        $audits->save();
    }
}
