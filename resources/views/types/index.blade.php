@extends('layouts.main')
@section('content')

    <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-block">
                            <table class="table table-striped table-hover table-responsive">
                                <thead>
                                <tr>
                                    <td colspan="4">
                                        <div align="center">
                                            <h1>{{ __('types.Types') }}</h1>
                                        </div>
                                        <div class="panel-heading">
                                            <a href="{{ route('types.add') }}" class="btn btn-blanco btn-new-job pull-right"> {{ __('types.Create Type Bidding') }}</a>
                                            <a href="{{ route('home') }}" class="btn btn-default" role="button"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ __('types.Back') }}</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>{{ __('types.Id') }}</th>
                                    <th>{{ __('types.Description') }}</th>
                                    <th>{{ __('types.DateTime') }}</th>
                                    <th colspan="2">{{ __('types.Actions') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($types as $type)
                                    <tr>
                                        <td>{{ $type->id }}</td>
                                        <td>{{ $type->description }}</td>
                                        <td>{{ Carbon\Carbon::parse($type->created_at)->format('l jS \\of F Y h:i:s A')  }}</td>
                                        <td><a href="{{ route('types.edit', $type->id) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                                        <td><a href="{{ route('types.delete', $type->id) }}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div align="center" style="width: 100%;">
                                {{ $types->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection