<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('searchings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip');
            $table->string('code_country')->nullable;
            $table->string('country')->nullable;
            $table->string('state')->nullable;
            $table->string('currency')->nullable;
            $table->text('search');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('searchings');
    }
}
