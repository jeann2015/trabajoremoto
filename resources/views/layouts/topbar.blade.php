<div class="navbar">

     <div class="navbar-item links">

     </div>
     <div class="navbar-item links">
         @if(Route::current()->getName() == 'home')
             {!! Form::open(['route' => 'home', 'method' => 'GET', 'role' => 'search']) !!}
         @else
             {!! Form::open(['route' => 'raiz', 'method' => 'GET', 'role' => 'search']) !!}
         @endif
            <div align="center" class="form-group">
                {!! Form::search('search', null, ['id' => 'mainSearch', 'type' => 'search', 'name' => 'search', 'placeholder' => 'Buscar...', 'autocomplete' => 'off']) !!}
            </div>
            <div style="visibility: hidden;">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
         {!! Form::close() !!}
     </div>
        @if (Auth::check())
            <div class="navbar-item links">
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle btn-blanco" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        {{ Auth::user()->name }}
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="{{ route('employer.profile')}}"><i class="fa fa-user" aria-hidden="true"></i> {{ __('topbar.Profile') }}</a></li>
                        <li role="separator" class="divider"></li>
                        @if(Auth::user()->isAdmin)
                            <li><a href="{{ route('employer') }}"><i class="fa fa-industry" aria-hidden="true"></i> {{ __('topbar.List Collaborator') }}</a></li>
                            <li><a href="/audit"><i class="fa fa-search" aria-hidden="true"></i> {{ __('topbar.Logs') }}</a></li>
                            <li><a href="{{ route('types') }}"><i class="fa fa-check-square" aria-hidden="true"></i> {{ __('topbar.Types Bidding') }}</a></li>
                            <li role="separator" class="divider"></li>
                        @endif
                        <li><a href="{{ route('job.create') }}"><i class="fa fa-plus" aria-hidden="true"></i> {{ __('topbar.Create Bidding') }}</a></li>
                        <li><a href="{{ route('home') }}"><i class="fa fa-list" aria-hidden="true"></i> {{ __('topbar.My Bidding') }}</a></li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out" aria-hidden="true"></i> {{ __('topbar.Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        @else
            <div class="navbar-item links">
                <a href="{{ url('/login') }}">{{ __('topbar.Sign In') }}</a>

            </div>
            <div class="navbar-item links">

                <a href="{{ url('/register') }}">{{ __('topbar.Register') }}</a>
            </div>
        @endif
 </div>
