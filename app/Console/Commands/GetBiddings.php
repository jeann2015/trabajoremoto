<?php

namespace Trabajosremoto\Console\Commands;

use Illuminate\Console\Command;
use Trabajosremoto\Bidding;

class GetBiddings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'biddings:external {provider}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Search Biddings and Save Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws \JobApis\Jobs\Client\Exceptions\MissingParameterException
     */
    public function handle()
    {
        $b = new Bidding;

        $provider = $this->argument('provider');

        $this->line('Results: '.$b->ProcessingBiddings($provider));
    }
}
