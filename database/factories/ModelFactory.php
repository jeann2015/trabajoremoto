<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Trabajosremoto\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'url'            => $faker->unique()->url,
        'facebook'       => "https://facebook.com/".$faker->userName,
        'twitter'        => "https://twitter.com/".$faker->userName,
        'linkedin'       => "https://linkdedin.com/".$faker->userName,
        'instagram'      => "https://instagram.com/".$faker->userName,
        'slogan'         => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'bio'            => $faker->sentence($nbWords = 50, $variableNbWords = true),
        'status'         => 1,
        'password'       => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


$factory->define(Trabajosremoto\Post::class, function (Faker\Generator $faker){

    $tipotrabajo = [
        'Contrato', 'Indefinido', 'Freelance', null
    ];
    $tiempotrabajo = [
        'Por horas', 'Medio tiempo', 'Fulltime', 'Contrato', null
    ];

    return [
            'title'         => $faker->sentence($nbWords = 10, $variableNbWords = true),
            'que_busca'     => $faker->paragraph($nbSentences = 20, $variableNbSentences = true),
            'que_ofrece'    => $faker->paragraph($nbSentences = 10, $variableNbSentences = true),
            'como_aplicar'  => $faker->paragraph($nbSentences = 2, $variableNbSentences = true),
            'destacado'     => mt_rand(0,1),
            'tipotrabajo'   => $tipotrabajo[mt_rand(0,3)],
            'tiempotrabajo' => $tiempotrabajo[mt_rand(0,4)],
            'adminStatus'   => mt_rand(0,1),
            'status'        => mt_rand(0,1),
            'vigente_hasta' => \Carbon\Carbon::now()->addMonths(mt_rand(1,4)),
            'visto'         => mt_rand(1, 200),
            'user_id'       => mt_rand(1,4)
    ];

});


$factory->define(\Trabajosremoto\Visita::class, function (Faker\Generator $faker){
    return [
        'ip'            => $faker->ipv4,
        'code_country'  => $faker->countryCode,
        'country'       => $faker->country,
        'state'         => $faker->state,
        'currency'      => $faker->currencyCode,
        'post_id'       => mt_rand(1, 150)

    ];
});