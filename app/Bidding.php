<?php

namespace Trabajosremoto;

use Illuminate\Database\Eloquent\Model;
use JobApis\Jobs\Client\JobsMulti;
use JobApis\Jobs\Client\Providers\StackoverflowProvider;
use JobApis\Jobs\Client\Queries\StackoverflowQuery;


class Bidding extends Model 
{
    protected $table="biddings";

    protected $fillable = [
        'title',
        'location',
        'description',
        'type',
        'title',
        'how_to_apply',
        'company',
        'company_url',
        'company_logo',
        'url',
        'type_id',
        'external_id',
        'created_at'
    ];

    /**
     * @param int $who
     * @return string
     * @throws \JobApis\Jobs\Client\Exceptions\MissingParameterException
     */
    public function ProcessingBiddings($who = 0){

        $b = new Bidding;
        $result = "";
        $con=0;
        $words = (new Word)->where('status','=',1)->get();

        if($who == 1 || $who == 0){
            foreach ($words as $word) {
                $url = 'https://jobs.github.com/positions.json?description='.$word->description.'&location=&full_time=true';
                $json = json_decode(file_get_contents($url), true);
                AccountBidding::create(['word_id'=>$word->id,'quantity'=>count($json)]);
                foreach ($json as $j) {
                    if ($this->search($j['id'])) {
                        $b->create([
                            'title' => $j['title'],
                            'location' => $j['location'],
                            'type' => $j['type'],
                            'description' => $j['description'],
                            'how_to_apply' => $j['how_to_apply'],
                            'company' => $j['company'],
                            'company_url' => $j['company_url'],
                            'company_logo' => $j['company_logo'],
                            'url' => $j['url'],
                            'type_id' => 1,
                            'external_id' => $j['id']
                        ]);
                        $con++;
                    }
                }
            }
            $result .= "Github = ".$con." ";
        }

        if($who == 2 || $who == 0){

            $url = 'https://api.adzuna.com:443/v1/api/jobs/gb/search/20?app_id=e008d140&app_key=8e76f803a5170bd220dc035106cec967&title_only=';

            $con = 0;
            foreach ($words as $word) {
                $json = json_decode(file_get_contents($url.$word->description), true);
                AccountBidding::create(['word_id'=>$word->id,'quantity'=>count($json['results'])]);
                if( count($json['results']) > 0) {
                    foreach ($json['results'] as $j) {
                        if ($this->search($j['id']) && isset($j['company']['display_name'])) {
                            $b->create([
                                'title' => $j['title'],
                                'location' => $j['location']['display_name'],
                                'type' => "Full Time",
                                'description' => $j['description'],
                                'how_to_apply' => "Apply here: " . $j['redirect_url'],
                                'company' => $j['company']['display_name'],
                                'company_url' => $j['redirect_url'],
                                'company_logo' => "",
                                'url' => $j['redirect_url'],
                                'type_id' => 1,
                                'external_id' => $j['id']
                            ]);
                            $con++;
                        }
                    }
                }
            }
            $result .= "Adzuna = ".$con." ";
        }

        if($who == 3 || $who == 0){

            $query = new StackoverflowQuery();

            foreach ($words as $word) {
                $query->set('q', $word->description);
                $client = new StackoverflowProvider($query);
                $json = $client->getJobs();
                $contador = $json->count();
                AccountBidding::create(['word_id'=>$word->id,'quantity'=>$contador]);
                for ($i = 0; $i <= $contador; $i++) {
                    $j = $json->get($i);
                    if (isset($j->url)) {
                        $id_external = explode("/", $j->url);
                        if ($this->search($id_external[4])) {
                            $b->create([
                                'title' => $j->title,
                                'location' => $j->location,
                                'type' => "Full Time",
                                'description' => $j->description,
                                'how_to_apply' => "",
                                'company' => $j->company,
                                'company_url' => "",
                                'company_logo' => "",
                                'url' => $j->url,
                                'type_id' => 1,
                                'external_id' => $id_external[4]
                            ]);
                            $con++;
                        }
                    }
                }
            }

            $result .= "Stackoverflow = ".$con." ";
        }

        if($who == 4 || $who == 0){

            $providers = [
                'Careercast' => [],
                'Dice' => [],
                'Govt' => [],
                'Ieee' => [],
                'Jobinventory' => [],
                'Monster' => [],
            ];

            $options = [
                'maxAge' => 7,
                'maxResults' => 2000,
                'orderBy' => 'datePosted',
                'order' => 'desc',
            ];

            $providers_keys = array_keys($providers);

            foreach ($providers_keys as $provider) {
                $mount_biddings[$provider]=0;
            }

            foreach ($words as $word) {
                $client = new JobsMulti($providers);
                $client->setKeyword($word->description);

                $json = $client->getAllJobs($options);
                $contador = $json->count();
                AccountBidding::create(['word_id'=>$word->id,'quantity'=>$contador]);
                $source = "";
                $con=0;
                for ($i = 0; $i <= $contador; $i++) {
                    $j = $json->get($i);
                    if (isset($j->url)) {
                        if ($this->search($j->url)) {
                            $b->create([
                                'title' => $j->title,
                                'location' => $j->location,
                                'type' => "Full Time",
                                'description' => $j->description,
                                'how_to_apply' => "",
                                'company' => $j->company,
                                'company_url' => "",
                                'company_logo' => "",
                                'url' => $j->url,
                                'type_id' => 1,
                                'external_id' => $j->url
                            ]);
                            if($source==""){$source = $j->source;$con++;}else{
                                if($source == $j->source){
                                    $con++;
                                }else{$source = $j->source; $result .= $source." = ".$con." "; $con=1;}
                            }
                        }
                    }
                }

            }
        }

        return $result;
    }

    /**
     * @param $external_id
     * @return bool
     */
    protected function search($external_id){
        $b = Bidding::where('external_id',$external_id)->get();
       if($b->count()>0){return false;}else{return true;}
    }


}
