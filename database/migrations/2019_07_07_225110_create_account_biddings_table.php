<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountBiddingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_biddings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quantity');
            $table->integer('word_id')->unsigned();
            $table->timestamps();
            $table->foreign('word_id')->references('id')->on('words');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_biddings');
    }
}
