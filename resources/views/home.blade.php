@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ __('home.My Bidding') }}
                    <a href="{{ route('job.create') }}" class="btn btn-blanco btn-new-job pull-right"> {{ __('home.Create Bidding') }}</a>
                </div>

                @if($posts->count() > 0)
                    <div class="panel-body">
                    <table class="table table-stripe table-hover">
                        <thead>
                        <td width="4%"></td>
                        <td width="71%"><strong>Titulo</strong></td>
                        <td width="5%"><strong>Visto</strong></td>
                        <td width="10%"><strong>Expira</strong></td>
                        <td width="10%" colspan="5"><strong>Opciones</strong></td>
                        </thead>

                        @foreach($posts as $post)
                            @if($post->destacado)
                                <tr class="destacado">
                            @else
                                <tr>
                            @endif
                                    <td>
                                    @if(!$post->status)
                                            <i class="fa fa-circle disabled" aria-hidden="true"></i>
                                    @endif
                                    </td>
                                    <td>{{ $post->title }}</td>
                                    <td>{{ $post->visto }}</td>
                                    <td>{{ Carbon\Carbon::parse($post->vigente_hasta)->format('d-m-Y') }}</td>
                                    <td><a href="{{ route('job.show', $post->slug) }}"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                                    <td><a href="{{ route('job.edit', $post->slug) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                                    <td>
                                        {{ Form::open(['route' => ['job.delete', $post->slug], 'method' => 'DELETE', 'class' => 'form-horizontal', 'id' => 'job'.$post->id ]) }}
                                        {{ Form::token() }}
                                        <input type="hidden" value="{{$post->slug}}" name="slug">
                                        <button type="submit" class="btn btn-blanco" id="delete">
                                            <a href="javascript:;" onclick="areYouSure(); return false;"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        </button>
                                        {{ Form::close() }}
                                    </td>
                                    <td>
                                        @if($post->destacado)
                                            <a href="javascript:;">
                                                <i class="fa fa-star yellow link-disable" aria-hidden="true"></i>
                                            </a>
                                        @else
                                            <a href="#">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                            </a>
                                        @endif

                                    </td>
                                    <td><a href="#"><i class="fa fa-level-up" aria-hidden="true"></i></a></td>
                                </tr>
                        @endforeach
                    </table>
                </div>
                @else
                    <div class="panel-body" align="center">
                        <h3>Intenta completar el perfil del empleador, esto atraera a mas aspirantes a ver la oferta.
                            <br>
                            No tienes ofertas todavia. </h3>
                        <a href="{{ route('job.create') }}" class="btn btn-blanco btn-new-job">Crear mi primera oferta</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@if($posts->count() > 0)
    <script>
        function areYouSure() {
            flag = confirm("Esta acción no se podrá revertir, estas seguro que quieres continuar?");
            if(!flag){ return false;}
            document.getElementById({{'job'.$post->id}}).submit()
         }
    </script>
@endif
@endsection
