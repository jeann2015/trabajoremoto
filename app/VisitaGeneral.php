<?php

namespace Trabajosremoto;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class VisitaGeneral extends Model
{
    protected $table="visita_generals";

    protected $fillable = [
        'ip',
        'code_country',
        'country',
        'state',
        'currency'
    ];

    /**
     * @param $ip
     */
    static public function guardarVisita($ip)
    {
        $visita = [];
        $info = geoip($ip);
        try{

            if(!isset($_COOKIE['visited'])) {

                $cookie_name = "visited";
                $cookie_value = true;
                setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
                $visita['ip'] = $ip;
                $visita['code_country'] = $info->iso_code;
                $visita['country'] = $info->country;
                $visita['state'] = $info->state_name;
                $visita['currency'] = $info->currency;
                VisitaGeneral::create($visita);
            }

        } catch (Exception $e) {
            Log::info($e);
            return;
        }

    }
}
