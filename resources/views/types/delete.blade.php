@extends('layouts.main')

@section('content')

    <div class="show-container-flex">


        {{-- Informacion del empleador--}}
        <div class="show-info-employer">
            <div class="show-info-employer-logo">
                <img src="{{ Storage::url($user->logo) }}" alt="">
            </div>
            <span class="show-info-employer-name">{{$user->name}}</span>
            <span class="show-info-employer-slogan">{{$user->slogan}}</span>
            <span class="show-info-employer-social">
                @if(isset($user->facebook))
                    <a href="{{ $user->facebook }}" target="_blank">
                        <i class="fa fa-2x fa-facebook-official" aria-hidden="true"></i>
                    </a>
                @endif
                @if(isset($user->twitter))
                    <a href="{{ $user->twitter }}" target="_blank">
                        <i class="fa fa-2x fa-twitter-square" aria-hidden="true"></i>
                    </a>
                @endif
                @if(isset($user->linkedin))
                    <a href="{{ $user->linkedin }}" target="_blank">
                        <i class="fa fa-2x fa-linkedin-square" aria-hidden="true"></i>
                    </a>
                @endif
                @if(isset($user->instagram))
                    <a href="{{ $user->instagram }}" target="_blank">
                        <i class="fa fa-2x fa-instagram" aria-hidden="true"></i>
                    </a>
                @endif

                @if(isset($user->url))
                    <a href="{{ $user->url }}" target="_blank">
                        <i class="fa fa-2x fa-globe" aria-hidden="true"></i><br>
                        <span class="show-info-employer-social-url">{{ $user->url }}</span>
                    </a>
                @endif
            </span>
        </div>

        {{-- Informacion de la oferta --}}
        <div class="show-info-job">
            <div class="card">
                <div class="panel-heading">
                    <a href="{{ route('types') }}" class="btn btn-default" role="button"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</a>
                </div>
            </div>

            {!! Form::open(['route' => ['types.destroy'], 'method' => 'POST', 'class' => 'form-horizontal' ]) !!}
            {{ Form::token() }}
            <h5 class="show-info-job-subtitle">{{ __('types.Description') }}</h5>
            {!! Form::text('description', $types->description, ['class' => 'form-control ', 'autocomplete' => 'off', 'placeholder' =>  __('types.Description') ]) !!}<br>
            @if ($errors->has('description'))
                <span class="help-block-red">
                    {{ $errors->first('description') }}
                </span>
            @endif

            <div class="form-group">
                <div class="col-md-12" align="center">
                    <button type="submit" class="btn btn-danger margintop30">
                        {{ __('types.Delete') }}
                    </button>
                    <input type="hidden" id="id" name="id" value="{{ $types->id }}">
                </div>
            </div>

        {!! Form::close() !!}
        </div>
    </div>
@endsection