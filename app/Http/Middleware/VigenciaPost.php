<?php

namespace Trabajosremoto\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Exception;
use Illuminate\Http\Request;
use Trabajosremoto\Post;

class VigenciaPost
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $post = Post::findBySlug($request->slug);
        if (is_null($post)){
            return redirect()->route('home')->withErrors(['Error' => 'Algo salio mal... No conseguimos este trabajo']);
        }

        if ($post->vigente_hasta < Carbon::now()){
            return redirect()->route('raiz')->withErrors(['Error' => 'El trabajo que estabas buscando ha expirado']);;
        }

        return $next($request);
    }
}
