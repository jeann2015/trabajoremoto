<footer>
    <div class="footer-info">
        <div class="footer-1">
            <div class="footer-logo">
                Trabajos Remoto
            </div>
        </div>
        <div class="footer-2">
            <div class="footer-derechos-info">
                {{ __('footer.right') }}
            </div>
        </div>
        <div class="footer-3">
            <div class="footer-socialmedia">
                <div class="socialmedia-1">
                    <a href="">
                        <i class="fa btn-lg fa-fw fa-facebook"></i>
                    </a>
                </div>
                <div class="socialmedia-2">

                    <a href="https://twitter.com/trabajosremoto">
                        <i class="fa btn-lg fa-fw fa-twitter"></i>
                    </a>
                </div>
                <div class="socialmedia-3">
                    <a href="">
                        <i class="fa btn-lg fa-fw fa-linkedin"></i>
                    </a>
                </div>
            </div>
        </div>

    </div>

</footer>