<?php

return [
    'Types' => 'Tipos de Ofertas',
    'Id' => 'Id',
    'Description' => 'Descripción',
    'DateTime' => 'Fecha/Hora',
    'Actions' => 'Acciones',
    'Create Type Bidding' => 'Create Tipo de Oferta',
    'Back' => 'Regresar',
    'Update' => 'Modificar',
    'Delete' => 'Borrar',
];