@extends('layouts.main')

@section('content')
    <div class="contenedor-principal">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {{-- Logo --}}
                        <div class="form-group view-profile">
                            <div class="col-md-3">
                                @if(isset($employer->logo))
                                    <img width="180px" height="180px" src="{{ Storage::url( $employer->logo) }}">
                                @else
                                    <img width="180px" height="180px">
                                @endif
                            </div>
                            <div class="col-md-9">
                                <label id="name" class="view-profile-name"> {{ $employer->name }}</label>
                            </div>

                            @if(isset($employer->slogan))
                                <div class="col-md-9">
                                    <label id="name" class=""> {{ $employer->slogan }}</label>
                                </div>
                            @endif

                            <div class="col-md-9">
                                <div class="oferta-social">
                                    @if(isset($employer->facebook))
                                        <a href="{{ $employer->facebook }}" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                                    @endif
                                    @if(isset($employer->twitter))
                                        <a href="{{ $employer->twitter }}" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                                    @endif
                                    @if(isset($employer->linkedin))
                                        <a href="{{ $employer->linkedin }}" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                                    @endif
                                    @if(isset($employer->instagram))
                                        <a href="{{ $employer->instagram }}" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    @endif
                                    @if(isset($employer->url))
                                        <a href="{{ $employer->url }}" target="_blank"><i class="fa fa-globe" aria-hidden="true"></i> {{ $employer->url }}</a>
                                    @endif
                                </div>
                            </div>

                            @if(isset($employer->bio))
                                <div class="col-md-12">
                                    <p name="bio" id="bio" class="">{{ $employer->bio }}</p>
                                </div>
                            @endif

                            <div class="col-md-12">
                                <span class="show-info-employer-share">
                                <div class="show-info-employer-share-box">
                                <strong>Comparte con tus amigos:</strong>&nbsp;&nbsp;&nbsp;
                                    <div class="sharethis-inline-share-buttons"></div>
                                </div>
                            </span>
                            </div>

                            <div class="col-md-12">
                                @if(count($employer->posts) > 0)
                                    <div class="">Mis ofertas</div>
                                    @foreach( $empleos as $post)
                                        <a href=" {{ route('job.show', $post->slug) }} " class="oferta-link">
                                            @if($post->destacado)
                                                <div class="oferta destacado">
                                                    @else
                                                        <div class="oferta">
                                                            @endif
                                                            <div class="oferta-logo">
                                                                @if( isset($post->user->logo))
                                                                    <img width="90px" height="90px" src="{{ Storage::url( $post->user->logo) }}">
                                                                @else
                                                                    <div style="height: 90px; width: 90px;"></div>
                                                                @endif
                                                            </div>
                                                            <div class="oferta-info">
                                                                <span class="oferta-titulo">{{ $post->title }}</span>
                                                                <br>
                                                                <span><a href="{{ route('job.show', $post->slug) }}">Ver empleo...</a></span>
                                                            </div>
                                                            <div class="oferta-caracteriscas">
                                                                <span>{{ $post->tipotrabajo }}</span>
                                                                <span>{{ $post->tiempotrabajo }}</span>
                                                            </div>
                                                        </div>
                                        </a>
                                    @endforeach
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=597923f480bb1d0011ab6e15&product=inline-share-buttons"></script>
@endsection
















