<?php

return [
    'Audits' => 'Auditorias',
    'Back' => 'Regresar',
    'Id' => 'Id',
    'User' => 'Usuario',
    'Description' => 'Descripción',
    'DateTime' => 'Fecha/Hora',
];