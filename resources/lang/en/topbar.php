<?php

return [
    'Profile'=>'Profile',
    'List Collaborator' => 'List Employer',
    'Logs' => 'Logs',
    'Types Bidding' => 'Types Bidding',
    'Create Bidding' => 'Create Bidding',
    'My Bidding' => 'My Bidding',
    'Logout' => 'Logout',
    'Sign In' => 'Sign In',
    'Register' => 'Register',
];
