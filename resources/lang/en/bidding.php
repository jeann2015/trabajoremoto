<?php

return [
    'Title' => 'Title Bidding',
    'TitleDescription' => 'Example: Developer Senior Java EE',
    'Conditions' => 'Conditions',
    'Select' => 'Select',
    'Hired' => 'Hired',
    'Undefined' => 'Undefined',
    'Freelancer' => 'Freelancer',
    'Per Hours' => 'Per Hours',
    'Middle Time' => 'Middle Time',
    'Fulltime' => 'Fulltime',
    'Contract' => 'Contract',
    'Looking' => 'What we Looking for?',
    'DescriptionLooking' => 'Write here the skills and abilities that the applicant should have, the functions that will be fulfilled as well as any other information that you think may be useful for the applicant.',
    'We Offer' => 'What we offer to you?',
    'DescriptionWeOffer' => 'Place here the information about the benefits offered by the traveler as well as the salary package. Try to be as interesting as possible for the applicant.',
    'How Apply' => 'How Apply?',
    'HowApplyDescription' => 'Express here the main contact data so that applicants can send their CVs, for example: "Please send CV to email@domain.com"',
    'Highlight' => ' Highlight the Bidding?',
    'Actions' => 'Actions',
    'Create Bidding' => 'Create Bidding',
    'Back' => 'Back',
    'Update' => 'Update',
    'Delete' => 'Delete',

];