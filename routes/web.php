<?php



App::setLocale("es");

Auth::routes();

Route::get('/audit', 'AuditsController@index');

Route::get('/', 'HomeController@raiz')->name('raiz');
Route::get('/job/{slug}', 'PostsController@show')->name('job.show')->middleware('esvigentepost');
Route::get('/bidding/show/{id}', 'BiddingController@show')->name('bidding.show')->middleware('esvigentebidding');


// Ver Perfil Empleador
Route::get('employer/{slug}', 'UsersController@view')->name('employer.view');


// Pagina inicial -> index
Route::get('/home', 'HomeController@index')->name('home');


/**
 * Employeers Routes
 */
Route::middleware(['auth'])->group(function () {

    // Mostrar Perfil
    Route::get('profile/{id?}', 'UsersController@showProfile')->name('employer.profile');

    // Actualizar Perfil
    Route::put('employer/update/{id?}', 'UsersController@updateProfile')->name('employer.update');

    // Listar Empleadores
    Route::get('employer', 'UsersController@listEmployer')->name('employer');


    Route::get('types', 'TypeBiddingController@index')->name('types');
    Route::get('/add', 'TypeBiddingController@add')->name('types.add');
    Route::post('/news', 'TypeBiddingController@news')->name('types.news');
    Route::get('edit/{id}', 'TypeBiddingController@edit')->name('types.edit');
    Route::post('update', 'TypeBiddingController@update')->name('types.update');
    Route::get('delete/{id}', 'TypeBiddingController@delete')->name('types.delete');
    Route::post('destroy', 'TypeBiddingController@destroy')->name('types.destroy');

    // Actualizar Trabajo
    Route::get('/job/edit/{slug}', 'PostsController@edit')->name('job.edit')->middleware('ownerPost');

    // Actualizar Trabajo
    Route::put('/job/update/{slug}', 'PostsController@update')->name('job.update')->middleware('ownerPost');

    // Eliminar Trabajo
    Route::delete('/job/delete/{slug}', 'PostsController@delete')->name('job.delete')->middleware('ownerPost');

    // Crear Trabajo
    Route::get('/new', 'PostsController@create')->name('job.create');

    // Guardar Trabajo
    Route::post('/new', 'PostsController@save')->name('job.save');
});

