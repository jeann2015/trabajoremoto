<?php

return [
    'My Profile' => 'My Profile',
    'Principal Information' => 'Principal Information',
    'Logo' => 'Logo',
    'Status' => 'Status',
    'Email' => 'E-Mail',
    'Company' => 'Company',
    'Slogan' => 'Slogan',
    'Us' => 'We',
    'Password' => 'Password',
    'Password Confirm' => 'Password Confirm',
    'Social Network' => 'Social Network',
    'Delete' => 'Delete',
    'Update' => 'Update',
    'Security' => 'Security',
    'Active' => 'Active',
    'No Active' => 'No Active',

];