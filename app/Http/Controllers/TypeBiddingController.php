<?php

namespace Trabajosremoto\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Trabajosremoto\Audits;
use Trabajosremoto\Http\Requests\TypesBiddingRequest;
use Trabajosremoto\Http\Requests\TypesBiddingUpdateRequest;
use Trabajosremoto\TypeBidding;

class TypeBiddingController extends Controller
{
    /**
     * TypeBiddingController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $audits_news = new Audits;

        $iduser = Auth::id();
        $url = $request->path();

        $audits_news->save_audits('Views Types Bidding');

        $types = DB::table('type_biddings')
            ->orderBy('id', 'desc')
            ->paginate(15);

        return view('types.index',compact('types'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        $user = Auth::user();
        return view('types.create',compact('user'));
    }

    /**
     * @param TypesBiddingRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function news(TypesBiddingRequest $request)
    {
        try {
            $audits_news = new Audits;

            $types = TypeBidding::create([
                'description' => $request->description
            ]);

            $audits_news->save_audits('Add Types Bidding, ID:'.$types->id);

            return redirect()->route('types')->withErrors(['error' => 'Enhorabuena! Has creado un Tipo']);
        }
        catch (\Exception $e) {
            return redirect()->route('types')->withErrors(['error' => 'Algo anda Mal']);
        }

    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $user = Auth::user();
        $types = TypeBidding::find($id);
        return view('types.edit',compact('user','types'));
    }

    /**
     * @param TypesBiddingUpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(TypesBiddingUpdateRequest $request)
    {
        try {

            $audits_news = new Audits;

            TypeBidding::where('id',$request->id)
                ->update(['description'=>$request->description]);

            $audits_news->save_audits('Update Types Bidding, ID:'.$request->id);

            return redirect()->route('types')->withErrors(['error' => 'Enhorabuena! El Registro se Modifico!']);
        } catch (\Exception $e) {
            return redirect()->route('types')->withErrors(['error' => 'Algo anda Mal']);
        }

    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete($id)
    {
        $user = Auth::user();
        $types = TypeBidding::find($id);
        return view('types.delete',compact('user','types'));
    }

    public function destroy(TypesBiddingUpdateRequest $request)
    {
        try {

            $audits_news = new Audits;

            TypeBidding::where('id',$request->id)
                ->delete();

            $audits_news->save_audits('Delete Types Bidding, ID:'.$request->id);

            return redirect()->route('types')->withErrors(['error' => 'Enhorabuena! El Registro se Elimino!']);
        } catch (\Exception $e) {
            return redirect()->route('types')->withErrors(['error' => 'Algo anda Mal']);
        }

    }


}
