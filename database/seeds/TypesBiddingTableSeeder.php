<?php

use Illuminate\Database\Seeder;
use Trabajosremoto\TypeBidding;


class TypesBiddingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TypeBidding::create(['description'=>'External']);
    }
}
