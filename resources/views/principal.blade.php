@extends('layouts.main')

@section('content')
    <div class="contenedor-principal">
        @foreach( $posts as $post)
            @if($post->type_post == 1)
                    <a href=" {{ route('job.show', $post->slug) }} " class="oferta-link">
            @else
                    <a href=" {{ route('bidding.show', $post->id) }} " class="oferta-link">
            @endif

            @if($post->destacado)
                <div class="oferta destacado">
            @else
                <div class="oferta">
            @endif
                    @if($post->type_post == 1)
                        <div class="oferta-logo">
                            @if( isset($post->logo) && $post->logo<>"" )
                                <img width="90px" height="90px" src="{{ Storage::url( $post->logo) }}">
                            @else
                                <div style="height: 90px; width: 90px;"></div>
                            @endif
                        </div>
                    @else
                        <div class="oferta-logo">
                            @if( isset($post->logo) && $post->logo<>"" )
                                <img width="90px" height="90px" src="{{  $post->logo }}">
                            @else
                                <div style="height: 90px; width: 90px;"></div>
                            @endif
                        </div>
                    @endif

                    <div class="oferta-info">
                        <span class="oferta-titulo">{!! $post->title !!} </span>
                        <div class="oferta-empleador-info">
                            @if(isset($post->slogan))
                                - {{ $post->slogan }} {{ 'Posted: '.\Carbon\Carbon::parse($post->created_at)->format('Y-m-d H:i:s') }}
                            @endif</a>
                            <div class="oferta-social">
                                @if(isset($post->facebook))
                                    <a href="{{ $post->facebook }}" target="_blank">
                                        <i class="fa fa-facebook-official" aria-hidden="true"></i>
                                    </a>
                                @endif
                                @if(isset($post->twitter))
                                    <a href="{{ $post->twitter }}" target="_blank">
                                        <i class="fa fa-twitter-square" aria-hidden="true"></i>
                                    </a>
                                @endif
                                @if(isset($post->linkedin))
                                    <a href="{{ $post->linkedin }}" target="_blank">
                                        <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                                    </a>
                                @endif
                                @if(isset($post->instagram))
                                    <a href="{{ $post->instagram }}" target="_blank">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                @endif

                                @if(isset($post->url))
                                    <a href="{{ $post->url }}" target="_blank">
                                        <i class="fa fa-globe" aria-hidden="true"></i>
                                        {{ $post->url }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="oferta-caracteriscas">
                        <span>{{ $post->tipotrabajo }}</span>
                        <span>{{ $post->tiempotrabajo }}</span>
                    </div>
                </div>

        @endforeach
            <br>
            <br>
            @if($posts->count() ==0)
                <div class="panel-body" align="center">
                    <h2>{{ __('principal.empty') }} </h2>

                    <br>
                    <br>
                    <br>

                </div>
            @endif
        <div align="center">
            {{ $posts->appends(['search' => Request::get('search')])->links() }}
        </div>
    </div>
@endsection

