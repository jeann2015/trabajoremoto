<?php

return [
    'Types' => 'Types Bidding',
    'Id' => 'Id',
    'Description' => 'Description',
    'DateTime' => 'DateTime',
    'Actions' => 'Actions',
    'Create Type Bidding' => 'Create Type Bidding',
    'Back' => 'Back',
    'Update' => 'Update',
    'Delete' => 'Delete',

];