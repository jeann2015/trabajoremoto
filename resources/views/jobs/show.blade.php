@extends('layouts.main')

@section('content')
    <div class="show-container-flex">
        {{-- Informacion del empleador--}}
        <div class="show-info-employer">
            <div class="show-info-employer-logo">
                <img src="{{ Storage::url($post->user->logo) }}" alt="">
            </div>
            <span class="show-info-employer-name">{{$post->user->name}}</span>
            <span class="show-info-employer-slogan">{{$post->user->slogan}}</span>
            <span class="show-info-employer-social">
                @if(isset($post->user->facebook))
                    <a href="{{ $post->user->facebook }}" target="_blank">
                        <i class="fa fa-2x fa-facebook-official" aria-hidden="true"></i>
                    </a>
                @endif
                @if(isset($post->user->twitter))
                    <a href="{{ $post->user->twitter }}" target="_blank">
                        <i class="fa fa-2x fa-twitter-square" aria-hidden="true"></i>
                    </a>
                @endif
                @if(isset($post->user->linkedin))
                    <a href="{{ $post->user->linkedin }}" target="_blank">
                        <i class="fa fa-2x fa-linkedin-square" aria-hidden="true"></i>
                    </a>
                @endif
                @if(isset($post->user->instagram))
                    <a href="{{ $post->user->instagram }}" target="_blank">
                        <i class="fa fa-2x fa-instagram" aria-hidden="true"></i>
                    </a>
                @endif

                @if(isset($post->user->url))
                    <a href="{{ $post->user->url }}" target="_blank">
                        <i class="fa fa-2x fa-globe" aria-hidden="true"></i><br>
                        <span class="show-info-employer-social-url">{{ $post->user->url }}</span>
                    </a>
                @endif

                @if(Auth::check())
                    @if($post->user_id === Auth::user()->id)
                        <div id="mychart" style="height: auto; width: 60%;"></div>
                        <script>
                            Morris.Bar({
                                element: 'mychart',
                                data: {!!$visitas!!},
                                xkey: 'country',
                                ykeys: ['total'],
                                labels: ['Visita']
                            });
                        </script>
                    @endif
                @endif
            </span>
            <span class="show-info-employer-share">
                <strong>Comparte este link:</strong>
                <div class="show-info-employer-share-box">
                    <div class="sharethis-inline-share-buttons"></div>
                </div>
            </span>
        </div>

        {{-- Informacion de la oferta --}}
        <div class="show-info-job">
            <span class="show-info-job-title">{{$post->title}}</span>
            <div style="width: 100%">
                <span class="show-info-job-posted pull-left">
                    @if(isset($post->tipotrabajo) && isset($post->tiempotrabajo))
                        {{ $post->tipotrabajo }} - {{ $post->tiempotrabajo }}
                    @elseif(isset($post->tipotrabajo))
                        {{ $post->tipotrabajo }}
                    @elseif(isset($post->tiempotrabajo))
                        {{ $post->tiempotrabajo }}
                    @endif
                </span>
                <span class="show-info-job-posted pull-right">Publicado desde {{ Carbon\Carbon::parse($post->created_at)->format('d/m/Y') }}</span>
            </div>
            <h4>{{$post->user->name}}</h4>
            <span class="show-info-employer-bio">{{ $post->user->bio }}</span>
            <hr>
            <h5 class="show-info-job-subtitle"><i class="fa fa-search" aria-hidden="true"></i> Que buscamos en ti?</h5>
            <span class="show-info-job-mainbody">{{ $post->que_busca }}</span>
            <hr>
            <h5 class="show-info-job-subtitle"><i class="fa fa-trophy" aria-hidden="true"></i> Lo que te ofrecemos.</h5>
            <span class="show-info-job-mainbody">{{ $post->que_ofrece }}</span>
            <hr>
            <h5 class="show-info-job-subtitle"><i class="fa fa-plane" aria-hidden="true"></i> Como aplicar?</h5>
            <span class="show-info-job-mainbody">{{ $post->como_aplicar }}</span>
            <hr>
            <div class="show-info-job-share-box">
                <div class="sharethis-inline-share-buttons"></div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=597923f480bb1d0011ab6e15&product=inline-share-buttons"></script>
@endsection