<?php

namespace Trabajosremoto\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Trabajosremoto\Audits;
use Trabajosremoto\Http\Requests\EmployerUpdateRequest;
use Trabajosremoto\Post;
use Trabajosremoto\User;

class UsersController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('view');
    }

    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showProfile($id = null)
    {

        if (isset($id)){
            if (Auth::user()->isAdmin) {
                $iduser = \Auth::id();
                return view('employers.profile',compact('iduser'))->with('user', User::find($id));
            } else {
                return Redirect::route('home')->withErrors(['error' => 'No tienes privilegios para modificar el perfil de otros empleadores']);
            }
        }

        return view('employers.profile')->with('user', Auth::user());
    }

    public function updateProfile(EmployerUpdateRequest $request, $id=null)
    {

        if (isset($id)){
            if (Auth::user()->isAdmin){
                $u = User::find($id);
            } else {
                return Redirect::route('home')->withErrors(['error' => 'No se puedes modificar los perfiles de otros empleadores']);
            }
        } else {
            $u = User::find(Auth::user()->id);
        }


        if ($request->hasFile('logo')){
            $logo = $request->file('logo')->store('public');
        }


        if ($u->isAdmin && ($request->status == 0)){
            return Redirect::route('home')->withErrors(['error' => 'No se puede deshabilitar a un empleador administrador']);
        }

        $u->name      = $request->name;
        $u->slug      = null;
        $u->email     = $request->email;
        $u->slogan    = $request->slogan;
        $u->bio       = $request->bio;
        $u->logo      = (isset($logo)) ? $logo : $u->logo;
        $u->password  = bcrypt($request->password_confirmation);
        $u->url       = $request->url;
        $u->facebook  = $request->facebook;
        $u->twitter   = $request->twitter;
        $u->linkedin  = $request->linkedin;
        $u->instagram = $request->instagram;

        $u->save();

        Audits::save_audits('Actualizo su perfil');

        return Redirect::route('home')->withErrors(['error' => 'Has actualizado tu perfil sin problemas.!']);
    }

    public function listEmployer(Request $request)
    {
        if (($request->user()->id === Auth::user()->id)
            && (Auth::user()->isAdmin)){
            $iduser = \Auth::id();
            return view('employers.list',compact('iduser'))->with('users', User::all());

        }

        return Redirect::route('home')->withErrors(['error' => 'No tienes privilegios para ver la lista de empleadores']);
    }

    public function view(Request $request)
    {
        $ahora = Carbon::now();


        $employer = User::findBySlug($request->slug);

        $empleos = Post::where('adminStatus', '=', 1)
            ->where('status', '=', 1)
            ->where('vigente_hasta', '>', $ahora)
            ->where('user_id', '=', $employer->id)
            ->get();

        if(is_null($employer)){
            return Redirect::route('employer')->withErrors(['error' => 'El empleador solicitado no existe.']);
        }

        $iduser = \Auth::id();

        return view('employers.view',compact('iduser'))
            ->with('employer', $employer)
            ->with('empleos', $empleos);
    }
}
