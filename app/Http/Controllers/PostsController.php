<?php

namespace Trabajosremoto\Http\Controllers;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Trabajosremoto\Audits;
use Trabajosremoto\Http\Requests\PostCreateRequest;
use Trabajosremoto\Http\Requests\UpdatePostRequest;
use Trabajosremoto\Post;
use Trabajosremoto\Visita;

class PostsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show(Request $request)
    {
        $iduser = Auth::id();

        $post = Post::findBySlugOrFail($request->slug);
        if (is_null($post)) {
            return redirect()->route('home')->withErrors(['Error' => 'Algo salio mal... No conseguimos este trabajo']);
        }

        $post->visto = ++$post->visto;
        $post->save();

        $ip = $request->server('REMOTE_ADDR');
        $this->guardarVisita($ip, $post->id);
        if (Auth::check()) {
            if (Auth::user()->id === $post->user_id) {
                $visitas = $this->getVisitsByPost($post->id);
                return view('jobs.show', compact('iduser', "post"))->with('visitas', $visitas);
            }
            Audits::save_audits('Vio sus post ' . $request->slug);
        }
        return view('jobs.show', compact("post"));
    }

    /**
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($slug)
    {
        $iduser = Auth::id();

        $post = Post::findBySlug($slug);
        if (is_null($post)) {
            return redirect()->route('home')->withErrors(['Error' => 'Algo salio mal... No conseguimos este trabjo']);
        }

        return view('jobs.edit', compact('iduser', 'post'));
    }

    /**
     * @param UpdatePostRequest $request
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdatePostRequest $request, $slug)
    {
        $post = Post::findBySlug($slug);
        $post->slug = null;
        $post->title = $request->title;
        $post->update(['title' => $request->title]);
        $post->tipotrabajo = $request->tipotrabajo;
        $post->tiempotrabajo = $request->tiempotrabajo;
        $post->que_busca = $request->que_busca;
        $post->que_ofrece = $request->que_ofrece;
        $post->como_aplicar = $request->como_aplicar;
        $post->status = $request->status;
        $post->save();

        Audits::save_audits('Actualizo su post '. $post->slug);
        return redirect()->route('home')->withErrors(['error' => 'El trabajo fue actualizado correctamente']);
    }

    /**
     * @param $slug
     * @return \Illuminate\Http\RedirectResponse
     * @throws Exception
     */
    public function delete($slug)
    {
        $post = Post::findBySlug($slug);
        if (is_null($post)) {
            return redirect()->route('home')->withErrors(['Error' => 'Algo salio mal... No conseguimos este trabjo']);
        }


        Audits::save_audits('Elimino su post '. $post->slug);
        $post->delete();
        return redirect()->route('home')->withErrors(['error' => 'El trabajo fue eliminado correctamente']);
    }

    public function create()
    {
        $iduser = Auth::id();

        $user = Auth::user();
        return view('jobs.create', compact('iduser', "user"));
    }

    /**
     * @param PostCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(PostCreateRequest $request)
    {
        $post = new Post();
        $post->title = $request->title;
        $post->tipotrabajo = $request->tipotrabajo;
        $post->tiempotrabajo = $request->tiempotrabajo;
        $post->que_busca = $request->que_busca;
        $post->que_ofrece = $request->que_ofrece;
        $post->como_aplicar = $request->como_aplicar;
        $post->vigente_hasta = Carbon::now()->addMonth(1);
        $post->user_id = $request->user()->id;
        $post->destacado = (isset($request->resaltar) && $request->resaltar == "on") ? 1 : 0;
        $post->save();

        Audits::save_audits('Creo un post, con ID: '.$post->id);

        return redirect()->route('home')->withErrors(['error' => 'Enhorabuena! Has creado el trabajo exitosamente']);
    }

    /**
     * @param $ip
     * @param $post_id
     */
    public function guardarVisita($ip, $post_id)
    {
        $visita = [];
        $info = geoip($ip);
        try {
            $visita['ip'] = $ip;
            $visita['code_country'] = $info->iso_code;
            $visita['country'] = $info->country;
            $visita['state'] = $info->state_name;
            $visita['currency'] = $info->currency;
            $visita['post_id'] = $post_id;

            $new = Visita::create($visita);
        } catch (Exception $e) {
            Log::info($e);
            return;
        }
    }

    /**
     * @param $post_id
     * @return mixed
     */
    public function getVisitsByPost($post_id)
    {
        $data = DB::table('visitas')
            ->select('country', DB::raw('count(*) as total'))
            ->where('post_id', '=', $post_id)
            ->groupBy('country')
            ->get();

        return $data->toJson();
    }
}
