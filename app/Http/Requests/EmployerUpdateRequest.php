<?php

namespace Trabajosremoto\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class EmployerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            //Informacion principal
            'logo'    => 'image|mimes:jpeg,png,jpg|max:2090|dimensions:max_width=260,max_height=260|max:2148',
            'email'   => 'required|email|min:5|unique:users,email,'.Auth::user()->id,
            'name'    => 'required|min:3',
            'slogan'  => 'required|min:3',
            'bio'     => 'required|min:250',

            //Seguridad
            'password'                => 'min:6',
            'password_confirmation'   => 'required_if:password,present|same:password|min:6',

            //Redes sociales
            'url'         => 'required|url|min:10',
            'facebook'    => 'url|string|nullable',
            'twitter'     => 'url|string|nullable',
            'linkedin'    => 'url|string|nullable',
            'instagram'   => 'url|string|nullable'
        ];
    }

    /**
     * @return string
     */
    public function getErrorBag()
    {
        dd($this->errorBag);
        return $this->errorBag;
    }
}
