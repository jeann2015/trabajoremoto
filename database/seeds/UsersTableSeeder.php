<?php

use Illuminate\Database\Seeder;
use Trabajosremoto\User;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         User::create([
            'name'      => 'Jean Carlos Nunez',
            'email'     => 'jeancarlosn2008@gmail.com',
            'slogan'    => 'Just we do it..!',
            'url'       => 'http://jeancarlosn.com',
            'facebook'  => "",
            'twitter'   => "",
            'linkedin'  => "",
            'instagram' => "",
            'status'    => 1,
            'isAdmin'   => 1,
            'bio'       => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic impedit maiores officia sint temporibus? Accusamus consequuntur, debitis delectus dolore earum explicabo facere, magnam nihil perferendis quam quis repellat reprehenderit voluptas. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic impedit maiores officia sint temporibus? Accusamus consequuntur, debitis delectus dolore earum explicabo facere, magnam nihil perferendis quam quis repellat reprehenderit voluptas.',
            'password'  => bcrypt('123Jeann123'),
            'remember_token' => str_random(10),
        ]);
        factory(User::class, 3)->create();
    }
}

