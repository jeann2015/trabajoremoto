<?php

namespace Trabajosremoto;

use Illuminate\Database\Eloquent\Model;

class TypeBidding extends Model
{
    protected $table="type_biddings";


    protected $fillable = [
        'description'
    ];
}
