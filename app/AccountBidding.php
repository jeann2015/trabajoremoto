<?php

namespace Trabajosremoto;

use Illuminate\Database\Eloquent\Model;

class AccountBidding extends Model
{
    protected $table="account_biddings";

    protected $fillable = [
        'word_id','quantity'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function word()
    {
        return $this->belongsTo('Trabajosremoto\Word');
    }
}
