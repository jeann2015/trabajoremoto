@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ __('employer.List Employer') }}
                    </div>

                    @if($users->count() > 0)
                        <div class="panel-body">
                            @foreach( $users as $user)
                                <div class="oferta-link">
                                @if($user->status)
                                    <div class="oferta" style="border: 1px solid lightgray;">
                                @else
                                    <div class="oferta destacado">
                                @endif
                                        <div class="oferta-logo">
                                            @if( isset($user->logo))
                                                <a class="oferta-titulo-a" href="{{ route('employer.view', $user->slug) }}">
                                                    <img width="90px" height="90px" src="{{ Storage::url( $user->logo) }}">
                                                </a>
                                            @else
                                                <div style="height: 90px; width: 90px;"></div>
                                            @endif
                                        </div>
                                        <div class="oferta-info">
                                            <span class="oferta-titulo"><a class="oferta-titulo-a" href="{{ route('employer.view', $user->slug) }}">{{ $user->name }}</a></span>
                                            <div class="oferta-empleador-info">
                                                @if(isset($user->slogan))
                                                     {{ $user->slogan }}
                                                @endif
                                                <div class="oferta-social">
                                                    @if(isset($user->facebook))
                                                        <a href="{{ $user->facebook }}" target="_blank">
                                                            <i class="fa fa-facebook-official" aria-hidden="true"></i>
                                                        </a>
                                                    @endif
                                                    @if(isset($user->twitter))
                                                        <a href="{{ $user->twitter }}" target="_blank">
                                                            <i class="fa fa-twitter-square" aria-hidden="true"></i>
                                                        </a>
                                                    @endif
                                                    @if(isset($user->linkedin))
                                                        <a href="{{ $user->linkedin }}" target="_blank">
                                                            <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                                                        </a>
                                                    @endif
                                                    @if(isset($user->instagram))
                                                        <a href="{{ $user->instagram }}" target="_blank">
                                                            <i class="fa fa-instagram" aria-hidden="true"></i>
                                                        </a>
                                                    @endif

                                                    @if(isset($user->url))
                                                        <a href="{{ $user->url }}" target="_blank">
                                                            <i class="fa fa-globe" aria-hidden="true"></i>
                                                            {{ $user->url }}
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="oferta-caracteriscas">
                                            <a href="{{ route('employer.profile', $user->id) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="panel-body" align="center">
                            <h3>
                                No hay empleadores registrados todavia.
                            </h3>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @if($users->count() > 0)
        <script>
            {{--function areYouSure() {--}}
                {{--flag = confirm("Esta acción no se podrá revertir, estas seguro que quieres continuar?");--}}
                {{--if(!flag){ return false;}--}}
                {{--document.getElementById({{'job'.$post->id}}).submit()--}}
            {{--}--}}
        </script>
    @endif
@endsection
