<?php

namespace Trabajosremoto\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Trabajosremoto\Bidding;

class VigenciaBidding
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $date_search = Carbon::now()->subDays(10)->toDateTimeString();
        $biddings = Bidding::find($request->id);
        if (is_null($biddings)){
            return redirect()->route('home')->withErrors(['Error' => 'Algo salio mal... No conseguimos este trabajo']);
        }

        if ($biddings->created_at < $date_search){
            return redirect()->route('raiz')->withErrors(['Error' => 'El trabajo que estabas buscando ha expirado']);;
        }
        return $next($request);
    }
}
