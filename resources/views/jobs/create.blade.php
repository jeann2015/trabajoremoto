@extends('layouts.main')

@section('content')
    <div class="show-container-flex">
        {{-- Informacion del empleador--}}
        <div class="show-info-employer">
            <div class="show-info-employer-logo">
                <img src="{{ Storage::url($user->logo) }}" alt="">
            </div>
            <span class="show-info-employer-name">{{$user->name}}</span>
            <span class="show-info-employer-slogan">{{$user->slogan}}</span>
            <span class="show-info-employer-social">
                @if(isset($user->facebook))
                    <a href="{{ $user->facebook }}" target="_blank">
                        <i class="fa fa-2x fa-facebook-official" aria-hidden="true"></i>
                    </a>
                @endif
                @if(isset($user->twitter))
                    <a href="{{ $user->twitter }}" target="_blank">
                        <i class="fa fa-2x fa-twitter-square" aria-hidden="true"></i>
                    </a>
                @endif
                @if(isset($user->linkedin))
                    <a href="{{ $user->linkedin }}" target="_blank">
                        <i class="fa fa-2x fa-linkedin-square" aria-hidden="true"></i>
                    </a>
                @endif
                @if(isset($user->instagram))
                    <a href="{{ $user->instagram }}" target="_blank">
                        <i class="fa fa-2x fa-instagram" aria-hidden="true"></i>
                    </a>
                @endif

                @if(isset($user->url))
                    <a href="{{ $user->url }}" target="_blank">
                        <i class="fa fa-2x fa-globe" aria-hidden="true"></i><br>
                        <span class="show-info-employer-social-url">{{ $user->url }}</span>
                    </a>
                @endif
            </span>
        </div>

        {{-- Informacion de la oferta --}}
        <div class="show-info-job">
        {!! Form::open(['route' => ['job.save'], 'method' => 'POST', 'class' => 'form-horizontal' ]) !!}
            {{ Form::token() }}
            <h5 class="show-info-job-subtitle">{{ __('bidding.Title') }}</h5>
            {!! Form::text('title', '', ['class' => 'form-control ', 'autocomplete' => 'off',
             'placeholder' =>  __('bidding.TitleDescription') ]) !!}<br>
            @if ($errors->has('title'))
                <span class="help-block-red">
                    {{ $errors->first('title') }}
                </span>
            @endif
            <h5 class="show-info-job-subtitle">{{ __('bidding.Conditions') }}</h5>
            <div style="width: 100%">
                <span class="show-info-job-posted pull-left">
                    {{ Form::select('tipotrabajo', [
                        null => __('bidding.Select'),
                        'Contratado' => __('bidding.Hired'),
                        'Indefinido' => __('bidding.Undefined'),
                        'Freelance'  => __('bidding.Freelancer'),
                    ], null, ['class' => 'form-control input-profile']) }}-
                    {{ Form::select('tiempotrabajo', [
                        null => __('bidding.Select'),
                        'Por horas'    => __('bidding.Per Hours'),
                        'Medio tiempo' => __('bidding.Middle Time'),
                        'Fulltime'     => __('bidding.Fulltime'),
                        'Contrato'     => __('bidding.Contract')
                    ], null, ['class'=>'form-control input-profile']) }}
                </span>
                @if ($errors->has('tipotrabajo'))
                    <span class="help-block-red">
                    {{ $errors->first('tipotrabajo') }}
                </span>
                @endif
                @if ($errors->has('tiempotrabajo'))
                    <span class="help-block-red">
                    {{ $errors->first('tiempotrabajo') }}
                </span>
                @endif<br>
            </div>
            <br>
            <br>
            <h5 class="show-info-job-subtitle">{{ __('bidding.Looking')}}</h5>
            {{ Form::textarea('que_busca', null, [
            'class' => 'form-control show-info-job-mainbody textarea-edit',
            'placeholder'=> __('bidding.DescriptionLooking')
            ] ) }}
            @if ($errors->has('que_busca'))
                <span class="help-block-red">
                    {{ $errors->first('que_busca') }}
                </span>
            @endif
            <hr>
            <h5 class="show-info-job-subtitle">{{ __('bidding.We Offer')}}</h5>
            {{ Form::textarea('que_ofrece', null, [
            'class' => 'form-control show-info-job-mainbody textarea-edit',
            'placeholder' => __('bidding.DescriptionWeOffer')
            ] ) }}
            @if ($errors->has('que_ofrece'))
                <span class="help-block-red">
                    {{ $errors->first('que_ofrece') }}
                </span>
            @endif
            <hr>
            <h5 class="show-info-job-subtitle"> {{ __('bidding.How Apply') }} </h5>
            {{ Form::textarea('como_aplicar', null, [
            'class' => 'form-control show-info-job-mainbody textarea-edit',
            'placeholder' => __('bidding.HowApplyDescription')
            ] ) }}
            @if ($errors->has('como_aplicar'))
                <span class="help-block-red">
                    {{ $errors->first('como_aplicar') }}
                </span>
            @endif
            <hr>

            <div class="destacado form-control">
                <label for="resaltar">
                    <input type="checkbox" id="resaltar" name="resaltar">
                    {{ __('bidding.Highlight') }}
                    {{--, solo por $ 2.99--}}
                </label>
            </div>

            <div class="form-group">
                <div class="col-md-12" align="center">
                    <button type="submit" class="btn btn-danger margintop30">
                        {{ __('bidding.Create Bidding') }}
                        {{-- por $ 9.99 --}}
                    </button>
                </div>
            </div>

        {!! Form::close() !!}
        </div>
    </div>
@endsection