<?php

namespace Trabajosremoto\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:10|max:60',
            'tipotrabajo' => 'required|in:Contratado,Indefinido,Freelance',
            'tiempotrabajo'=> 'required|in:Por horas,Medio tiempo,Fulltime,Contrato',
            'que_busca' => 'required|min:30|string',
            'que_ofrece' => 'required|min:20|string',
            'como_aplicar' => 'required|min:10|string'
        ];
    }
}
